# Sneakybox YKAE Editor

![YKAE Editor](docs/images/ykaeeditor.png)

## Installation

```shell
$ git lfs clone git@gitlab.com:sauls-gamedev/other/sneakybox-ykae-editor.git
```

## Features
* Isometric view of the room with 2 walls and floor
* Various furniture models + 3 primitives
* Possibility to move placed objects, by selecting them and move them by holding right mouse button
* Possibility to change object color
* Possibility to delete placed objects with delete button
* Possibility to toggle grid and/or snap to objects fucntionality
* New, Save, Load your creation
* Possibility to move objects up or down

## Usage

If you want to know the controls, help can be viewed by pressing `F1` on your keayboard

![Controls](docs/images/controls.png)

## Known issues

* When moving up or down, the grid is not applied
* Sometimes placing objects with snap enabled creates tham with a space
* When object is selected, selecting to place object, the selected outline is not removed

## What to improve
* Better snapping
* Better grid support
* Isometric camera zoom, movement, rotation
* ESC - could make back to edit mode from create mode