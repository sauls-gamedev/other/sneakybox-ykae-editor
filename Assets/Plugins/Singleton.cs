using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Component
{
    private static T _instance;

    public static T Instance => _instance;

    private void Awake() 
    {
        if (_instance != null && _instance != this) 
        { 
            Destroy(gameObject);
            return;
        }

        _instance = this as T;

        OnAwake();
    }

    protected virtual void OnAwake()
    {
    }
}