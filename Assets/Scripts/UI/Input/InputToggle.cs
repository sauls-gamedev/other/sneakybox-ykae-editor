using Runtime.Input;
using UnityEngine;

namespace UI.Input
{
    public class InputToggle : MonoBehaviour
    {
        private void OnEnable()
        {
            InputManager.Instance.Disable();
        }

        private void OnDisable()
        {
            InputManager.Instance.Enable();
        }
    }
}