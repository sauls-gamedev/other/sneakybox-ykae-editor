using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI.Button
{
    public class Hoverable : UIButton, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
    {
        [SerializeField] protected Color defaultColor;
        [SerializeField] protected Color hoverColor;
        
        protected Image _image;

        private void Start()
        {
            _image = GetComponent<Image>();
            _image.color = defaultColor;
        }

        public virtual void OnPointerEnter(PointerEventData eventData)
        {
            _image.color = hoverColor;
        }

        public virtual void OnPointerExit(PointerEventData eventData)
        {
            _image.color = defaultColor;
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            onClickEvent?.Invoke();
        }
    }
}