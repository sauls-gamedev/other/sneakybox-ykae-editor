using UnityEngine;
using UnityEngine.Events;

namespace UI.Button
{
    public class UIButton : MonoBehaviour
    {
        [SerializeField] public UnityEvent onClickEvent;
    }
}