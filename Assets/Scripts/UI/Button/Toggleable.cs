using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Button
{
    public class Toggleable : Hoverable
    {
        [SerializeField] private Color activeColor;
        [SerializeField] private bool isActive = false;
        

        public override void OnPointerExit(PointerEventData eventData)
        {
            _image.color = isActive ? activeColor : defaultColor;
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            
            onClickEvent?.Invoke();
            
            if (!isActive)
            {
                isActive = true;
                _image.color = activeColor;
            }
            else
            {
                isActive = false;
                _image.color = defaultColor;
            }
        }
    }
}