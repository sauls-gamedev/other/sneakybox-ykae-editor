using UnityEngine;

namespace UI.Button
{
    public class SelfHideable : MonoBehaviour
    {
        public void Hide()
        {
            gameObject.SetActive(!gameObject.activeSelf);
        }
    }
}