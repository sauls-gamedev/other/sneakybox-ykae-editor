using System;
using UnityEngine;

namespace UI.Objects
{
    [Serializable]
    [CreateAssetMenu(fileName = "Object", menuName = "Objects/Object", order = 1)]
    public class Object : ScriptableObject
    {
        [SerializeField] public GameObject prefab;
        [SerializeField] public string name;
    }
}