using Runtime.Object;
using Runtime.Object.Builder;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI.ListView
{
    public class ListItemController : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] public Image image;
        [SerializeField] public Color defaultColor;
        [SerializeField] public Color selectedColor;
        [SerializeField] public bool isSelected = false;

        private GameObject _objects;
        private ListItemData _itemData;
        private GameObject _objectSelection;
        private BuilderManager _builderManager;

        public void Start()
        {
            _objects = GameObject.FindWithTag("ObjectListContent");
            _itemData = GetComponent<ListItemData>();
            _objectSelection = GameObject.FindWithTag("ObjectSelection");
            _builderManager = BuilderManager.Instance;
            
            if (isSelected)
            {
                Select();
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Select();

            DeleteSelectionObjects();

            if (_itemData.name != "None")
            {
                var currentPosition = BuilderManager.Instance.GetPosition();
                var newPosition = new Vector3(
                    currentPosition.x,
                    currentPosition.y + _itemData.obj.prefab.transform.position.y,
                    currentPosition.z
                );

                var obj = Instantiate(
                    _itemData.obj.prefab,
                    newPosition,
                    Quaternion.identity,
                    _objectSelection.transform);
                
                _builderManager.SetActiveObject(obj, _itemData);
                _builderManager.SetCreateMode();

                obj.GetComponent<Outline>().OutlineMode = Outline.Mode.SilhouetteOnly;
                obj.AddComponent<Snapable>();
            }
            else
            {
                _builderManager.SetEditMode();
            }
        }

        private void DeleteSelectionObjects()
        {
            var childCount = _objectSelection.transform.childCount;
            if (childCount > 0)
            {
                for (var i = 0; i < childCount; i++)
                {
                    Destroy(_objectSelection.transform.GetChild(i).gameObject);
                }
            }
        }

        private void Select()
        {
            UnselectAll();
            image.color = selectedColor;
        }

        private void UnselectAll()
        {
            for (var i = 0; i < _objects.transform.childCount; i++)
            {
                var listItemController = _objects.transform.GetChild(i).gameObject.GetComponent<ListItemController>();
                listItemController.Unselect();
            }
        }

        private void Unselect()
        {
            image.color = defaultColor;
        }
    }
}