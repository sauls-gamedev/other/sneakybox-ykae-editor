using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using Object = UI.Objects.Object;

namespace UI.ListView
{
    public class ListItemData : MonoBehaviour
    {
        [SerializeField] public Object obj;
        [SerializeField] public int itemWidth = 128;
        [SerializeField] public int itemHeight = 128;
        
        private Image image;
        private TMP_Text text;

        protected void Start()
        {
            if (obj != null)
            {
                image = GetComponentsInChildren<Image>().FirstOrDefault(x => x.name == "ObjectPreview");
                text = GetComponentsInChildren<TMP_Text>().FirstOrDefault(x => x.name == "Name");
                
                image.sprite = Sprite.Create(
                    GetPrefabPreview(obj.prefab),
                    new Rect(0, 0, itemWidth, itemHeight), 
                    new Vector2()
                );
                text.text = obj.name;
            }
        }

        private Texture2D GetPrefabPreview(GameObject prefab)
        {
            string path = AssetDatabase.GetAssetPath(prefab);
            var editor = Editor.CreateEditor(prefab);
            Texture2D tex = editor.RenderStaticPreview(path, null, itemWidth, itemHeight);
            DestroyImmediate(editor);

            return tex;
        }
    }
}