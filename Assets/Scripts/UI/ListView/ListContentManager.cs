using Runtime.Object;
using UnityEngine;

namespace UI.ListView
{
    public class ListContentManager : MonoBehaviour
    {
        [SerializeField] private GameObject editor;
        [SerializeField] private GameObject listItemPrefab;
        [SerializeField] private GameObject content;

        private RoomObjectsCollection _roomObjectsCollection;
        
        private void Start()
        {
            _roomObjectsCollection = editor.GetComponent<AvailableObjects>().GetObjectsCollection();

            foreach (var (name, obj) in _roomObjectsCollection)
            {
                var listItem = Instantiate(
                    listItemPrefab, 
                    Vector3.zero, 
                    Quaternion.identity, 
                    content.transform
                );

                listItem.GetComponent<ListItemData>().obj = obj;
                listItem.name = obj.name;
            }
        }
    }
}