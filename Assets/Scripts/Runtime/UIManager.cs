using Runtime.Input;
using TMPro;
using UnityEngine;

namespace Runtime
{
    public class UIManager : Singleton<UIManager>
    {
        [SerializeField] private GameObject helpPanel;
        [SerializeField] private GameObject dialogBox;

        private InputManager _inputManager;

        private void Start()
        {
            _inputManager = InputManager.Instance;
        }

        private void Update()
        {
            if (_inputManager.HelpMenu.WasPerformedThisFrame())
            {
                helpPanel.SetActive(!helpPanel.activeSelf);
            }
        }

        public void ShowDialogBox(string titleText, string messageText, string okButtonText = "Ok")
        {
            var textFields = dialogBox.GetComponentsInChildren<TMP_Text>();
            foreach (var textField in textFields)
            {
                textField.text = textField.name switch
                {
                    "titleText" => titleText,
                    "messageText" => messageText,
                    "okButtonText" => okButtonText,
                    _ => textField.text
                };
            }
            dialogBox.SetActive(!dialogBox.activeSelf);
        }
    }
}