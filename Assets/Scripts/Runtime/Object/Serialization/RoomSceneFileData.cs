using System;
using System.Collections.Generic;

namespace Runtime.Object.Serialization
{
    [Serializable]
    public class RoomSceneFileData
    {
        public List<RoomObjectData> Objects;

        public RoomSceneFileData(List<RoomObjectData> objects)
        {
            Objects = objects;
        }
    }
}