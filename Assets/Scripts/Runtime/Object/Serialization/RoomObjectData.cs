using System;
using UnityEngine;

namespace Runtime.Object.Serialization
{
    [Serializable]
    public class RoomObjectData
    {
        public string Name;
        public Vector3 Position;
        public Quaternion Rotation;
        public Color Color;
    }
}