using UnityEngine;

namespace Runtime.Object
{
    public class AvailableObjects : MonoBehaviour
    {
        [SerializeField] private RoomObjectsCollection roomObjectsCollection;

        public RoomObjectsCollection GetObjectsCollection()
        {
            return roomObjectsCollection;
        }
    }
}