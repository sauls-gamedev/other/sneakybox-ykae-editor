using Runtime.Object.Builder;
using UnityEngine;

namespace Runtime.Object
{
    public class Snapable : MonoBehaviour
    {
        private BuilderManager _builderManager;
        
        [SerializeField] private float snapDistance = 1f;
        [SerializeField] private float snapRadius = 1f;

        private void Start()
        {
            _builderManager = BuilderManager.Instance;
        }
        void Update()
        {
            if (_builderManager.IsSnapEnabled())
            {
                Collider[] hitColliders = Physics.OverlapSphere(transform.position, snapRadius);
                foreach (var hitCollider in hitColliders)
                {
                    Vector3 myClosestPoint = GetComponent<Collider>().ClosestPoint(hitCollider.transform.position);
                    Vector3 targetClosestPoint = hitCollider.ClosestPoint(myClosestPoint);
                    Vector3 offset = targetClosestPoint - myClosestPoint;
                    if (offset.magnitude < snapDistance)
                    {
                        transform.position += offset;
                    }
                }
            }
        }
    }
}