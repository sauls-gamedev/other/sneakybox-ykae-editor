using System;
using RotaryHeart.Lib.SerializableDictionary;

namespace Runtime.Object
{
    [Serializable]
    public class RoomObjectsCollection : SerializableDictionaryBase<string, UI.Objects.Object>
    {
    }
}