using UnityEngine;

namespace Runtime.Object
{
    public class Selectable : MonoBehaviour 
    {
        public bool IsSelected { get; private set; } = false;

        public void Select()
        {
            IsSelected = true;
        }

        public void Unselect()
        {
            IsSelected = false;
        }
    }
}