namespace Runtime.Object.Builder
{
    public enum BuilderMode
    {
        Create,
        Edit,
    }
}