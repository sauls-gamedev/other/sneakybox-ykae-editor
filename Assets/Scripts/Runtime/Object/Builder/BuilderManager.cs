using JetBrains.Annotations;
using Runtime.Input;
using Runtime.Object.Serialization;
using UI.ListView;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Runtime.Object.Builder
{
    public class BuilderManager : Singleton<BuilderManager>
    {
        private Vector3 _position;
        private RaycastHit _raycastHit;
        private GameObject _activeObject;
        private ListItemData _placeableObjectItemData;
        private GameObject _roomObjects;
        private Vector3 _rotation;
        private Vector3 _movement;
        private RoomObjectsCollection _roomObjectsCollection;
        private BuilderMode _mode = BuilderMode.Edit;
        private InputManager _inputManager;

        [SerializeField] private FlexibleColorPicker colorPicker;
        [SerializeField] private LayerMask layerMask;
        [SerializeField] private float gridSize = 0.5f;
        [SerializeField] private bool isGridEnabled = false;
        [SerializeField] private float rotateSpeed = 1f;
        [SerializeField] private float moveSpeed = 1f;
        [SerializeField] private bool isSnapEnabled = false;
        
        private void Start()
        {
            _roomObjects = GameObject.FindGameObjectWithTag("RoomObjects");
            _roomObjectsCollection = GetComponent<AvailableObjects>().GetObjectsCollection();
            _inputManager = InputManager.Instance;
            _inputManager.Enable();
        }

        private void Update()
        {
            if (_activeObject != null)
            {
                if (IsCreateMode())
                {
                    _activeObject.transform.position = CalculateNextPosition();
                    CheckForObjectPlacement();
                }

                if (IsEditMode())
                {
                    var snapComponent = _activeObject.GetComponent<Snapable>();
                    if (_inputManager.MouseRightClickAction.IsPressed())
                    {
                        if (!snapComponent)
                        {
                            _activeObject.AddComponent<Snapable>();
                        }

                        _activeObject.transform.position = CalculateNextPosition();
                    }
                    else
                    {
                        if (snapComponent)
                        {
                            Destroy(snapComponent);
                        }
                    }

                    CheckForDelete();
                }

                CheckForObjectRotation();
                CheckForObjectMovement();
            }
        }

        private void CheckForObjectPlacement()
        {
            if (_inputManager.MouseLeftClickAction.WasPerformedThisFrame())
            {
                Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
                if (Physics.Raycast(ray, out var raycastHit, 100f))
                {
                    if (raycastHit.transform != null
                        && (raycastHit.transform.CompareTag("RoomFloor")
                            || raycastHit.transform.CompareTag("Selectable")
                        )
                       )
                    {
                        PlaceObject();
                    }
                }
            }
        }

        private void CheckForObjectRotation()
        {
            if (_inputManager.RotateRightAction.WasPerformedThisFrame() || _inputManager.RotateRightAction.IsPressed()) _rotation = Vector3.up;
            else if (_inputManager.RotateLeftAction.WasPerformedThisFrame() || _inputManager.RotateLeftAction.IsPressed())
                _rotation = Vector3.down;
            else _rotation = Vector3.zero;

            _activeObject.transform.Rotate(_rotation * rotateSpeed);
        }

        private void CheckForObjectMovement()
        {
            if (_inputManager.MoveUpAction.WasPerformedThisFrame() || _inputManager.MoveUpAction.IsPressed()) _movement = Vector3.up;
            else if (_inputManager.MoveDownAction.WasPerformedThisFrame() || _inputManager.MoveDownAction.IsPressed()) _movement = Vector3.down;
            else _movement = Vector3.zero;

            _activeObject.transform.position += _movement * moveSpeed;
        }
        
        private void CheckForDelete()
        {
            if (_inputManager.DeleteAction.WasPerformedThisFrame())
            {
                Destroy(_activeObject.gameObject);
                _activeObject = null;
            }
        }

        private Vector3 CalculateNextPosition()
        {
            var nextPosition = new Vector3(
                _position.x,
                _position.y + _activeObject.transform.position.y,
                _position.z
            );

            if (!isGridEnabled)
            {
                return nextPosition;
            }

            return new Vector3(
                RoundToNearestGridCell(nextPosition.x),
                isGridEnabled ? nextPosition.y : RoundToNearestGridCell(nextPosition.y),
                RoundToNearestGridCell(nextPosition.z)
            );
        }

        private void PlaceObject()
        {
            var placedObject = Instantiate(
                _placeableObjectItemData.obj.prefab,
                _activeObject.transform.position,
                _activeObject.transform.rotation,
                _roomObjects.transform
            );
               
            placedObject.GetComponent<MeshRenderer>().material.color = _activeObject.GetComponent<MeshRenderer>().material.color;
            placedObject.name = _placeableObjectItemData.obj.prefab.name;
        }

        public void Build(RoomObjectData objectData)
        {
            _roomObjectsCollection.TryGetValue(objectData.Name, out var obj);

            if (!obj) return;

            var placedObject = Instantiate(
                obj.prefab,
                objectData.Position,
                objectData.Rotation,
                _roomObjects.transform
            );

            placedObject.GetComponent<MeshRenderer>().material.color = objectData.Color;
            placedObject.name = objectData.Name;
        }

        private void FixedUpdate()
        {
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());

            if (Physics.Raycast(ray, out var raycastHit, 1000, layerMask))
            {
                _position = raycastHit.point;
            }
        }

        public Vector3 GetPosition()
        {
            return _position;
        }

        public void SetActiveObject(GameObject obj, ListItemData listItemData)
        {
            _activeObject = obj;
            _placeableObjectItemData = listItemData;
            UpdateActiveObjectColor(colorPicker.color);
        }

        public void SetActiveObject(GameObject obj)
        {
            _activeObject = obj;
            UpdateActiveObjectColor(colorPicker.color);
        }

        private float RoundToNearestGridCell(float pos)
        {
            float difference = pos % gridSize;
            pos -= difference;

            if (difference > (gridSize / 2))
            {
                pos += gridSize;
            }

            if (difference * (-1f) > (gridSize / 2))
            {
                pos -= gridSize;
            }

            return pos;
        }

        public void GridToggle()
        {
            isGridEnabled = !isGridEnabled;
        }

        public void SnapToggle()
        {
            isSnapEnabled = !isSnapEnabled;
        }

        public void SetCreateMode()
        {
            _mode = BuilderMode.Create;
        }

        public bool IsCreateMode()
        {
            return _mode.Equals(BuilderMode.Create);
        }

        public void SetEditMode()
        {
            _mode = BuilderMode.Edit;
        }

        public bool IsEditMode()
        {
            return _mode.Equals(BuilderMode.Edit);
        }

        public bool HasActiveObject()
        {
            return _activeObject != null;
        }

        public GameObject GetActiveObject()
        {
            return _activeObject;
        }

        public void UpdateActiveObjectColor(Color color)
        {
            if (!HasActiveObject()) return;

            _activeObject.GetComponent<Renderer>().material.color = color;
        }

        public GameObject GetRoomObjects()
        {
            return _roomObjects;
        }

        public bool IsGridEnabled()
        {
            return isGridEnabled;
        }

        public bool IsSnapEnabled()
        {
            return isSnapEnabled;
        }
    }
}