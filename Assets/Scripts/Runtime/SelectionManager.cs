using System;
using Runtime.Input;
using Runtime.Object.Builder;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Runtime
{
    public class SelectionManager : MonoBehaviour
    {
        private RaycastHit _raycastHit;
        private Outline _lastObjectOutline;
        private BuilderManager _builderManager;
        private InputManager _inputManager;
        private GameObject _selection;

        [SerializeField] private FlexibleColorPicker _colorPicker; 

        private void Start()
        {
            _builderManager = BuilderManager.Instance;
            _inputManager = InputManager.Instance;
        }

        private void Update()
        {
            if (_lastObjectOutline != null)
            {
                _lastObjectOutline.OutlineMode = Outline.Mode.SilhouetteOnly;
                _lastObjectOutline = null;
            }

            if (_builderManager.HasActiveObject() && _builderManager.IsEditMode())
            {
                _builderManager.GetActiveObject().GetComponent<Outline>().OutlineMode = Outline.Mode.OutlineAll;
            }
            
            if (!_builderManager.IsEditMode()) return;
            
            Ray ray = Camera.main.ScreenPointToRay(Mouse.current.position.ReadValue());
            
            if (!EventSystem.current.IsPointerOverGameObject() && Physics.Raycast(ray, out _raycastHit))
            {
                var obj = _raycastHit.transform;
                _lastObjectOutline = obj.GetComponent<Outline>();
                
                
                if (obj.CompareTag("Selectable"))
                {

                    if (_lastObjectOutline != null && !_lastObjectOutline.OutlineMode.Equals(Outline.Mode.OutlineAll))
                    {
                        _lastObjectOutline.OutlineMode = Outline.Mode.OutlineAll;
                    }

                    if (_inputManager.MouseLeftClickAction.WasPerformedThisFrame())
                    {
                        _builderManager.SetActiveObject(null);
                        _colorPicker.color = obj.GetComponent<MeshRenderer>().material.color;
                        _builderManager.SetActiveObject(obj.gameObject);
                        UnselectAll(obj.gameObject);
                    }
                }
                else
                {
                    if (_lastObjectOutline != null)
                    {
                        _lastObjectOutline.OutlineMode = Outline.Mode.SilhouetteOnly;
                        _lastObjectOutline = null;
                    }
                }
            }
        }

        private void UnselectAll(GameObject selectedObject)
        {
            var roomObjects = _builderManager.GetRoomObjects();
            for (var i = 0; i < roomObjects.transform.childCount; i++)
            {
                var obj = roomObjects.transform.GetChild(i).gameObject;
                if (obj == selectedObject) continue;

                obj.GetComponent<Outline>().OutlineMode = Outline.Mode.SilhouetteOnly;
            }
        }
    }
}