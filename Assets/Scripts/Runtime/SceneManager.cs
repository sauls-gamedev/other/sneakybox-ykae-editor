using System;
using System.Collections.Generic;
using System.IO;
using Runtime.Object.Builder;
using Runtime.Object.Serialization;
using UnityEditor;
using UnityEngine;

namespace Runtime
{
    public class SceneManager : MonoBehaviour
    {
        [SerializeField] private GameObject roomObjects;
        private string _currentFilename = "";

        public void New()
        {
            for (var i = 0; i < roomObjects.transform.childCount; i++)
            {
                Destroy(roomObjects.transform.GetChild(i).gameObject);
            }
            _currentFilename = "";
        }

        public void Save()
        {
            if (_currentFilename == "")
            {
                _currentFilename = EditorUtility.SaveFilePanel(
                    "Save Scene",
                    "",
                    "newfile.ykae",
                    "ykae"
                );

                if (!IsFileSelected())
                {
                    return;
                }
            }

            StreamWriter writer = new StreamWriter(_currentFilename);

            var roomObjectsData = new List<RoomObjectData>();

            for (var i = 0; i < roomObjects.transform.childCount; i++)
            {
                var currentObject = roomObjects.transform.GetChild(i).transform;
                var roomObjectData = new RoomObjectData();
                roomObjectData.Name = currentObject.gameObject.name;
                roomObjectData.Position = currentObject.position;
                roomObjectData.Rotation = currentObject.rotation;
                roomObjectData.Color = currentObject.GetComponent<MeshRenderer>().material.color;

                roomObjectsData.Add(roomObjectData);
            }

            writer.Write(JsonUtility.ToJson(new RoomSceneFileData(roomObjectsData)));

            writer.Close();

            UIManager.Instance.ShowDialogBox(
                "Save successful",
                "Scene successfully saved to " + _currentFilename,
                "Ok"
            );
        }


        public void Load()
        {
            _currentFilename = EditorUtility.OpenFilePanel("Load Scene", "", "ykae");

            if (!IsFileSelected())
            {
                return;
            }

            try
            {
                StreamReader reader = new StreamReader(_currentFilename);
                var roomSceneFileData = JsonUtility.FromJson<RoomSceneFileData>(reader.ReadToEnd());
                reader.Close();

                New();

                foreach (var obj in roomSceneFileData.Objects)
                {
                    BuilderManager.Instance.Build(obj);
                }
            }
            catch (Exception e)
            {
                UIManager.Instance.ShowDialogBox("Error", "Failed to open file. \n " + e.Message, "Ok");
                _currentFilename = "";
            }
        }

        private bool IsFileSelected()
        {
            if (_currentFilename == "")
            {
                UIManager.Instance.ShowDialogBox("Error", "File was not selected!", "Ok");
                return false;
            }

            return true;
        }
    }
}