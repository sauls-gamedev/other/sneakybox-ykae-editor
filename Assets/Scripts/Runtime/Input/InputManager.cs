using UnityEngine.InputSystem;

namespace Runtime.Input
{
    public class InputManager : Singleton<InputManager>
    {
        private InputActions _inputActions;
        public InputAction RotateLeftAction { get; private set; }
        public InputAction RotateRightAction { get; private set; }
        public InputAction MouseLeftClickAction { get; private set; }
        public InputAction MoveUpAction { get; private set; }
        public InputAction MoveDownAction { get; private set; }
        public InputAction DeleteAction { get; private set; }
        public InputAction MouseRightClickAction { get; private set; }
        public InputAction HelpMenu { get; private set; }

        protected override void OnAwake()
        {
            _inputActions = new();
        }

        protected void Start()
        {
            RotateLeftAction = _inputActions.Editor.RotateLeft;
            RotateRightAction = _inputActions.Editor.RotateRight;
            MouseLeftClickAction = _inputActions.Editor.MouseLeftClick;
            MoveUpAction = _inputActions.Editor.MoveUp;
            MoveDownAction = _inputActions.Editor.MoveDown;
            DeleteAction = _inputActions.Editor.Delete;
            MouseRightClickAction = _inputActions.Editor.MouseRightClick;
            HelpMenu = _inputActions.Editor.HelpMenu;
        }

        public void Enable()
        {
            if (_inputActions != null) 
                _inputActions.Enable();
        }

        public void Disable()
        {
            if (_inputActions != null) 
                _inputActions.Disable();
        }
    }
}